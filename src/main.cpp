/*
	SUMMARY: wszystkie zrobione poza 10
*/


#include <iostream>
#include <map>

using namespace std;

int WyszukajMaksimum(int* tab, int size);
int GdzieJestMinimum(int *tab, int size);
int* PosortujTablice(int* tab, int size, bool ascending = true);
float ObliczSrednia(int* tab, int size);
float ObliczSredniaWazona(int* tab, int size);
float Mediana(int* tab, int size);
int ZnajdzMode(int* tab, int size);

int main()
{
	// 01
	float toJestMojaZmienna = 3.1415;

	// 03
	int mojaTablica[] = { 1, 10, 2, 9, 3, 8, 4, 7, 5, 6};
	int tab2[] = { 5, 5, 8, 8, 8, 1, 2, 9, 5, 8, 9 , 0};
	//#[test]
	cout << "Moda:\t" << ZnajdzMode(tab2, sizeof(tab2) / sizeof(int)) <<"\n";
}

// 04
int WyszukajMaksimum(int* tab, int size) {
	int max = tab[0];
	for(int i = 1; i < size; i++) {
		if(tab[i] > max) {
			max = tab[i];
		}
	}
	return max;
}

// 05
int GdzieJestMinimum(int *tab, int size) {
	int minimumIndex = 0;
	int minimum = tab[0];
	for(int i = 0; i < size; i++) {
		if(tab[i] < minimum) {
			minimum = tab[i];
			minimumIndex = i;
		}
	}
	return minimumIndex;
}

// 06
int* PosortujTablice(int* tab, int size, bool ascending) {
	for(int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			if(ascending) {
				if(tab[i] < tab[j]) {
					int temp = tab[i];
					tab[i] = tab[j];
					tab[j] = temp;
				}
			}
			else {
				if(tab[i] > tab[j]) {
					int temp = tab[i];
					tab[i] = tab[j];
					tab[j] = temp;
				}
			}
		}
	}
	return tab;
}

// 07
float ObliczSrednia(int *tab, int size) {
	int sum = 0;
	for(int i = 0; i < size; i++) {
		sum += tab[i];
	}
	return sum / size;
}

// 08
float ObliczSredniaWazona(int* tab, int size) {
	float sum = 0;
	int elements = size;
	for(int i = 0; i < size; i++) {
		if(tab[i] % 2) { // nieparzyste
			sum += tab[i];
		}
		else {			// parzyste
			sum += (2 * tab[i]);
			elements += 1;
		}
	}

	return sum / elements;
}

// 09
float Mediana(int* tab, int size) {
	// sort the array
	tab = PosortujTablice(tab, size);

	if(size % 2) { // nieparzyste
		return tab[(size + 1) / 2 - 1];
	}
	else { // parzyste
		return ((float)(tab[size / 2 - 1] + tab[size / 2]) / 2.0);
	}
}

// 10
int ZnajdzMode(int* tab, int size) {
	map<int, int> occurences;

	for(int i = 0; i < size; i++) {
		occurences[tab[i]]++;
	}

	cout << "\n";
	for(auto& it: occurences) {
		cout << it.first << ": " << it.second << "\n";
	}

	auto biggest = occurences.begin();
	for(auto it: occurences) {
		if(it.second > biggest->second) {
			biggest = it;
		}
	}

	return 0;
}
